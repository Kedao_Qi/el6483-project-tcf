#PROJECT REPORT - EL6483 (Spring 2015)
=========================================

##Optimal Temperature control and alert while testing temperature sensitive electronics boards in continuous test mode.

## Team

- Kedao Qi <mailto:kq286@nyu.edu>

- Parth Ranjan Singh <mailto:prs331@nyu.edu>

- Rose Merlyn Prakash <mailto:rmp433@nyu.edu>



##BACKGROUND
------------
The motivation behind this Project is to control cooling of electronic boards on a rack when the system gets heated up during testing.
The group members are working in Power Lab and constantly have the problem of system failure due to improper thermal management,especially during continuous testing.
So, the Project is a miniature prototype for cooling system/proper thermal managment with optimized power usage.This will also sense if temperature of any critical board/IC rises beyond a threshold and cuts off power. This will ensure safety of Lab while we do over-night testings which is very common.

The idea throughout the project was to create a simple controller that can be easily incorporated into any existing system/rack in the Lab.


##DESIGN
----------------------------

The block diagram for our Project given below:
 ![Block Diagram](http://s6.postimg.org/z7ayu2yy9/block_diagram.jpg)

Here, the temperature sensors would be pasted on the chip or board where we need to sense the temperature.The sensed Voltage values are given to the ADC on Discovery board. The values are then converted to equivalent temperature values. Based on the temperature and the required state machine (shown below), the system controls Fan control through PWM.Also, if the temperature exceeds critical thresholds, alarm/trip signals are generated.

Design requirements are:
- The system should be optimal mode once powered ON with Speed-1 for the fan(Optimal speed)
- When temperature is above T1(threshold),it should increase the fan speed to Speed-2
- When temperature is above T2(crtical), it should increase the fan speed to Speed-3(Maximum Speed) and generate alarm.
- When temperature is above T3(danger), it should generate the trip signal
- System should remain to be in Trip state if temperature rises to danger level, until user resets the system.
These requirements are handled by the software as decribed in the sections below.

Here, we have used the STM32F Discovery board to implement the above.
The concepts required for understanding this Project are: (Links are provided to the materials and EL6483 labs with similar concepts,for reference):

- General understanding of STM32F Architecture [STM32F Reference Manual](http://witestlab.poly.edu/~ffund/el6483/files/DM00031020.pdf).
- How to use LEDS onboard/General GPIOs [Lab for GPIO](https://bitbucket.org/el6483s2015/el6483-lab/src/7c4130697704678665b5235c8fde473cad2a76c2/1-blinky/?at=master).
- How to use Push button onboard [Lab for GPIO](https://bitbucket.org/el6483s2015/el6483-lab/src/7c4130697704678665b5235c8fde473cad2a76c2/2-peripherals/?at=master).
- How to use ADC on-board and concepts of ADC [Lab for ADC](https://bitbucket.org/el6483s2015/el6483-lab/src/7c4130697704678665b5235c8fde473cad2a76c2/3-adc-temp/?at=master), [ADC explained for Arduino](https://learn.sparkfun.com/tutorials/analog-to-digital-conversion).
- How to use generate PWM using timers onboard and concepts of PWM[Lab for PWM](https://bitbucket.org/el6483s2015/el6483-lab/src/7c4130697704678665b5235c8fde473cad2a76c2/5-timing-interrupts/?at=master), [PWM concept](https://learn.sparkfun.com/tutorials/pulse-width-modulation).
- Implementation of State Machines and interrupts [Lab for State Machines](https://bitbucket.org/el6483s2015/el6483-lab/src/7c4130697704678665b5235c8fde473cad2a76c2/5-timing-interrupts/?at=master).


##HARDWARE
------------

The STM32F4 Discovery board's ARM processor is the primary microcontroller in our project.

List of materials used and the link to the webpage from where it was purchases is given below:

- [STM32F Discovery board](http://www.digikey.com/catalog/en/partgroup/stm32f4-evaluation-board-stm32f4discovery/18797)
- [24 V DC Fan](http://www.digikey.com/product-detail/en/ME80252V2-000U-A99/259-1507-ND/2021135)
- [DC Power Supply](http://www.amazon.com/Tekpower-Linear-TP1502D-Digital-Display/dp/B00MMOC9N8) (Power supply for the motor driver circuit and fan.)
- [External temparature sensors](http://www.digikey.com/product-detail/en/MCP9700A-E%2FTO/MCP9700A-E%2FTO-ND/1212508)
- [Motor shield]('http://arduino.cc/en/Main/ArduinoMotorShieldR3') (Direction control of the motor(fan) and also providing isolation to the microcontroller)
- [Relay]('http://www.digikey.com/product-detail/en/9007-05-00/306-1062-ND/301696') (Enable the Alarm and Trip Circuits as the supervisory control)

Wiring the hardware is as below:

![Wiring Diagram](http://i.imgur.com/aR1Mymh.jpg)

-The first peripheral we setup is the Temperature sensor as it is the base for the logic circuit. Here, the temperature sensor directly gives out voltages. SO, we can connect it to the ADC thorugh a resistor.The pin 'PC0' pin on board for the interface.
Caution:: Connect a resistor before sensor if you're using a 5V source because you might burn the device though the data sheet for the thermistor says it is not required (Because We DID burn the sensor at first!!)
Note: Here, since we are directly getting volatage,we could connect it directly to the ADC.As required during a testing, We have also tried taking out the themistor output integrated on the IGBT of a critical Power device which gave accurate values and it worked great!Ofcourse, in such cases, you need to add an additional potential divider circuit so that ADC can measure the voltage.See [Reading Resistive Sensors](https://learn.sparkfun.com/tutorials/voltage-dividers) for clarity).

-The second peripheral is the PWM generation for Motor Control. The Board generates the PWM output and it is connected to the drivers for the Motor.The pin 'PB6' on board in used for the interface.
 PWM is used to control the speed of Fan. The fan we have used here is driven by 24V. However,since the board output levels are of 3V, a driver needs to be used. We have selected L293D as the driver circuits. It pulls the PWM output to the voltage levels of the Power Supply.

-The third peripheral is for Relays. The Board generates signals to the relays which in turn switches the alarm and trip signal.The pin 'PC14' and 'PC15' onboard are used for the interfaces.
 Relays we have selected is SPST(Single Pole Single Throw)- 2 separate for 2 different output signals namely Alarm and Trip signal.Here, for our prototype, we have indicated the signals using LEDs. However, in the actual implementation, this signal may be read back by the control boards of the EUT(Equipment Under Test) to take appropriate actions like power cutoff etc.
 
Pin Mapping is:

 Discovery board | Function                             |
-----------------|--------------------------------------|
 GND             | Ground                               |
 3V              | Power                                |
 PC0             | Analog input from Temperature sensor |
 PB6             | PWM output to the driver of Motor    |
 PC14            | Signal to Relay1 for Alarm           |
 PC15            | Signal to Relay2 for Trip            |


##SOFTWARE
-----------

######Folder Details:
/common : Standard Libraries used in the Project
/testing : Individual modules used for testing - ADC Module, PWM module , Relay Module
/final : Integrated Module

####Step 1 : Initialization of all modules with proper configuration

We have used the standard STM Libraries which are located in the souce file folders.

The first thing to do is initialization of all modules as shown below. Here, we have initialized and configured the GPIOs/interfaces for Temperarure sensing(ADC), PWM, Relay and System timer.
To understand more about how we configured the interfaces, refer the configuration registers in [STM32F Reference Manual](http://witestlab.poly.edu/~ffund/el6483/files/DM00031020.pdf). Also, the comments in the source code clearly tells how we have done the configuration.

Initialization calls are shown below: 

```````````````````````````````````````````

        InitSystick();
        init_leds();
        init_adc();
        init_timer_pins();
        init_timer();
        init_pwm();
        init_button();
        init_relay();
        GPIO_SetBits(GPIOC, RELAY1);
        GPIO_SetBits(GPIOC, RELAY2);
````````````````````````````````````````
The initialization functions could be found in the source file.

####Step 2 - Implementation of State Machine

State machine for the system is

![State Machine](http://s6.postimg.org/vk82qhx0x/STATEDIAGRAM.jpg)

It is implemented as shown below:


````````````````````````````````````````````````

  read_adc();

  switch(state)
  {
    case OPTIMAL:
        GPIO_SetBits(GPIOD, GPIO_Pin_12); //green LED
        GPIO_SetBits(GPIOC, RELAY2); //Reset Trip LED
	GPIO_SetBits(GPIOC, RELAY1); //Reset Alarm LED
        if (Temperature > OPTIMAL_TEMP){
            TIM4->CCR1=HEATED_SPEED ;//speed-2 (75% of max speed)
            state = HEATED;
	}
    break;

    case HEATED:
        GPIO_SetBits(GPIOD, GPIO_Pin_13);  //orange LED
        GPIO_SetBits(GPIOC, RELAY2); //Reset Trip LED
  	    GPIO_SetBits(GPIOC, RELAY1); //Reset Alarm LED
        if (Temperature > HEATED_TEMP){
           GPIO_SetBits(GPIOC, RELAY2); //Reset Trip
           GPIO_ResetBits(GPIOC, RELAY1); //Alarm Indication
	   state = OVERHEAT;
           TIM4->CCR1= OVERHEATED_SPEED;//speed-3 (100% of max speed)
        }
	else if (Temperature < OPTIMAL_TEMP){
          state=OPTIMAL;
          TIM4->CCR1= OPTIMAL_SPEED; //speed-1 (50% of max speed)
          GPIO_SetBits(GPIOC, RELAY1);
        }
    break;

    case OVERHEAT:
       GPIO_SetBits(GPIOD, GPIO_Pin_14);  //RED led
       if (Temperature > OVERHEATED_TEMP){
          GPIO_ResetBits(GPIOC, RELAY1); //Alarm signal
          GPIO_ResetBits(GPIOC, RELAY2); //Trip Signal
          state= TRIP;
       }
       else if (Temperature < HEATED_TEMP){
          GPIO_SetBits(GPIOC, RELAY2); //Reset Trip LED
	  state = HEATED;
          TIM4->CCR1= HEATED_SPEED; //Speed-2 (75% of max speed)
       }
    break;

   case TRIP:
       GPIO_SetBits(GPIOD, GPIO_Pin_15); //blue LED
       if (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)){
          state=OPTIMAL;
          GPIO_SetBits(GPIOC, RELAY1);
	  GPIO_SetBits(GPIOC, RELAY2);
       }
   break;

   default:
        TIM4->CCR1=OPTIMAL_SPEED; //speed-1(50% of max speed)
        state = OPTIMAL;
        break;
````````````````````````````````````````````````

The system switches between OPTIMAL,HEATED,OVERHEATED and TRIP as per the state machine requirements.
Also,it is ensured that the system stays in TRIP mode until user button is pressed.

####To run the Project
In the Terminal, navigate to the location of the Makefile, which is `/home/ubuntu/el6483-project-tcf/final`. Do `make` to compile the code. 
And then do `make flash` to load the code in to the board. Press the reset button on the board and then it is ready to run.  


##TEST RESULTS
---------------

Test was done first on PCB and results were satisfactory.
It was used in Power lab for sensing the temperature from the Power modules.Setup is shown below:



=======
![Setup](http://i.imgur.com/Qu8TewP.jpg)


#DISCUSSION
------------

While implementing the Project, the following points need to be taken care of:

1. Remember to connect a resistor before temperature sensor if you're using a 5V source because you might burn the device though the data sheet for the thermistor says it is not required (Because We DID burn the sensor at first!!)
Note: Here, since we are directly getting volatage,we could connect it directly to the ADC.As required during a testing, We have also tried taking out the themistor output integrated on the IGBT of a critical Power device which gave accurate values and it worked great!Ofcourse, in such cases, you need to add an additional potential divider circuit so that ADC can measure the voltage.See [Reading Resistive Sensors](https://learn.sparkfun.com/tutorials/voltage-dividers) for clarity).

2. Remember that the STM board can only give up to 3.3V for the PWM output.So,Use proper drivers for driving the Motors. Here, for driving a Fan with 24V, we had to use the appropriate drivers.

3. Ensure proper Grounding.
 
