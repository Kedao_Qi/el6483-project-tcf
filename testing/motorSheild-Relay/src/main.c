#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

void init();
void loop();

void initRelay();
void initButton();


int main() {
    init();

    do {
        loop();
    } while (1);
}

void init() {
    initRelay();
    initButton();
}

void loop()
{
//The relay operates at 5V whereas our Digital Out pin provides only 3 V, So when disabled Relay Enable Pin = 5-3V and when enabled Relay Enable Pin = 5-0V
    if (((GPIOA->IDR)&(0x0001))==1)
    {
        // Use BSRRH, set bits of GPIOC pins 15 LOW
        GPIOC->BSRRH=(0x8000);
    }
    else
    {
        // Use BSRRL, set bits of GPIOC pins 15 HIGH
        GPIOC->BSRRL=(0x8000);
    }
}

void initRelay()
{
    // Enable GPIOC Clock
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
    // Set GPIOC pins 15 mode to output
    GPIOC->MODER |= (1<<(15<<1));
}

void initButton() {
    // Enable GPIOA Clock
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
    // Set GPIOA pin 0 mode to input
    GPIOA->MODER &= 0xFFFFFFFC;
}
