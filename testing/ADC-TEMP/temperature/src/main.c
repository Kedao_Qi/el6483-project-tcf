#include <stdio.h>
#include "stm32f4xx.h"
#include "stm32f4xx_adc.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

const uint16_t LEDS = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
void init();
void loop();
void delay();

void leds_init() {
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	GPIO_InitTypeDef gpio;
	GPIO_StructInit(&gpio);
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(GPIOD, &gpio);
	}

void adc_init() {
	ADC_InitTypeDef ADC_init_structure; //Structure for adc confguration
	GPIO_InitTypeDef GPIO_initStructre; //Structure for analog input pin
	//Clock configuration
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE); //The ADC1 is connected the APB2 peripheral bus thus we will use its clock source
	RCC_AHB1PeriphClockCmd(RCC_AHB1ENR_GPIOCEN, ENABLE); //Clock for the ADC port 
	//Analog pin configuration
	GPIO_StructInit(&GPIO_initStructre);
	GPIO_initStructre.GPIO_Pin = GPIO_Pin_0; //The channel 10 is connected to PC0
	GPIO_initStructre.GPIO_Mode = GPIO_Mode_AN; //The PC0 pin is configured in analog mode
	GPIO_initStructre.GPIO_PuPd = GPIO_PuPd_NOPULL; //no pull up or pull down
	GPIO_Init(GPIOC, &GPIO_initStructre); //Affecting the port with the initialization structure configuration
	//ADC structure configuration
	ADC_DeInit();
	ADC_init_structure.ADC_DataAlign = ADC_DataAlign_Right; //data converted will be shifted to right
	ADC_init_structure.ADC_Resolution = ADC_Resolution_12b; //Input voltage is converted into a 12bit number giving a maximum value of 4096
	ADC_init_structure.ADC_ContinuousConvMode = ENABLE; //the conversion is continuous, the input data is converted more than once
	ADC_init_structure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1; // conversion is synchronous with TIM1 and CC1
	ADC_init_structure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None; //no trigger for conversion
	ADC_init_structure.ADC_NbrOfConversion = 1; 
	ADC_init_structure.ADC_ScanConvMode = DISABLE; //The scan is configured in one channel
	ADC_Init(ADC1, &ADC_init_structure); //Initialize ADC with the previous configuration

	//Select the channel to be read from
	ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 1, ADC_SampleTime_144Cycles);

        //Enable ADC conversion
	ADC_Cmd(ADC1, ENABLE);
	}

void Delay(unsigned int Val) {
	for (; Val != 0; Val--)
	__NOP();
	}

uint16_t readADC1(uint8_t channel) {
	ADC_SoftwareStartConv(ADC1);//Start the conversion
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC));//Processing the conversion
	return ADC_GetConversionValue(ADC1); //Return the converted data
	}

int main() {
	leds_init();
	adc_init();

        float Temperature_d;
	float Temperature_v ;
        float Temperature;
	do {
	Temperature_d = readADC1(16);

	/*Calculation for temperature*/
        Temperature_v = ((Temperature_d*3.3)/(float)4095);
        Temperature= (Temperature_v-0.4)/0.0195; //Sensitivity is 19.5 mV/degree celcius;400mV for 0 deg celcius

	setbuf(stdout, NULL);
	printf("Temperature Analog value is %f \n",Temperature_v);
	printf("Temperature Digital value is %f \n",Temperature_d);
        printf("Temperature value is %f \n",Temperature);


	#ifdef DEBUG
	if (Temperature_d > 1000 && Temperature_d < 1200) {
	GPIO_SetBits(GPIOD, GPIO_Pin_12);
	}
	else if (Temperature_d > 1200 && Temperature_d < 1300) {
	GPIO_SetBits(GPIOD, GPIO_Pin_13);
	}
	else if (Temperature_d > 1300 && Temperature_d < 1400) {
	GPIO_SetBits(GPIOD, GPIO_Pin_14);
	}
	else if (Temperature_d > 1400) {
	GPIO_SetBits(GPIOD, GPIO_Pin_15);
	}
	#endif
	
        if (Temperature > 30) {
	GPIO_SetBits(GPIOD, GPIO_Pin_12);
	}
	else if (Temperature > 40) {
	GPIO_SetBits(GPIOD, GPIO_Pin_13);
	}
	else if (Temperature_d > 50) {
	GPIO_SetBits(GPIOD, GPIO_Pin_14);
	}
	else if (Temperature_d > 60) {
	GPIO_SetBits(GPIOD, GPIO_Pin_15);
	} 

	for(int i=0;i<100;i++);

	Delay(50000);
	GPIO_ResetBits(GPIOD, GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);
	Delay(50000);

	} while (1);
	}
