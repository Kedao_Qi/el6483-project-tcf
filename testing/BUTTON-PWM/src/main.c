#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_tim.h"

int counter=0;
void TM_PINS_Init(void) {
    GPIO_InitTypeDef GPIO_InitStruct;
    
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
 
    /* Alternating functions for pins */
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_TIM4);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_TIM4);
    
    /* Set pins */
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOB, &GPIO_InitStruct);
}
 
void TM_TIMER_Init(void) {
    TIM_TimeBaseInitTypeDef TIM_BaseStruct;
    
    /* Enable clock for TIM4 */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
/*                                                            
    
    timer_tick_frequency = Timer_default_frequency / (prescaller_set + 1)        
    
    Timer prescaler = 0; 
    
    timer_tick_frequency = 84000000 / (0 + 1) = 84000000
*/    
    TIM_BaseStruct.TIM_Prescaler = 0;
    /* Count up */
    TIM_BaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
/*
    Timer Max Value: 16bit = 65535
    
    PWM_frequency = timer_tick_frequency / (TIM_Period + 1)
    
    TIM_Period = timer_tick_frequency / PWM_frequency - 1
    
    10Khz PWM_frequency, set Period to
    
    TIM_Period = 84000000 / 10000 - 1 = 8399
    
    If you get TIM_Period larger than max timer value (in our case 65535),
    you have to choose larger prescaler and slow down timer tick frequency
*/
    TIM_BaseStruct.TIM_Period = 8399; /* 10kHz PWM */
    TIM_BaseStruct.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_BaseStruct.TIM_RepetitionCounter = 0;
    /* Initialize TIM4 */
    TIM_TimeBaseInit(TIM4, &TIM_BaseStruct);
    /* Start count on TIM4 */
    TIM_Cmd(TIM4, ENABLE);
}
 
void TM_PWM_Init(void) {
    TIM_OCInitTypeDef TIM_OCStruct;
    
    /* PWM mode 2 = Clear on compare match */
    /* PWM mode 1 = Set on compare match */
    TIM_OCStruct.TIM_OCMode = TIM_OCMode_PWM2;
    TIM_OCStruct.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCStruct.TIM_OCPolarity = TIM_OCPolarity_Low;
    
/*
    
    pulse_length = ((TIM_Period + 1) * DutyCycle) / 100 - 1
    
    25% duty cycle:     pulse_length = ((8399 + 1) * 25) / 100 - 1 = 2099
    50% duty cycle:     pulse_length = ((8399 + 1) * 50) / 100 - 1 = 4199
    75% duty cycle:     pulse_length = ((8399 + 1) * 75) / 100 - 1 = 6299
    100% duty cycle:    pulse_length = ((8399 + 1) * 100) / 100 - 1 = 8399
    
    If pulse_length > TIM_Period, constant output HIGH.
*/
    TIM_OCStruct.TIM_Pulse = 0; /* PB6, 0% Duty Cycle as default */
    TIM_OC1Init(TIM4, &TIM_OCStruct);
    TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);
    
    TIM_OCStruct.TIM_Pulse = 8399; /* PB9, 100% Duty Cycle */
    TIM_OC4Init(TIM4, &TIM_OCStruct);
    TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);
}

void initButton() {
    
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
    	GPIO_InitTypeDef gpio;
	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = GPIO_Pin_0;
	gpio.GPIO_Mode = GPIO_Mode_IN;
	GPIO_Init(GPIOA, &gpio);

}
 
int main(void) {
    SystemInit();
    TM_PINS_Init();
    TM_TIMER_Init();
    TM_PWM_Init();
    initButton();
 
   while (1) {
 
        while ((!GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0))&&(counter==0))
        {TIM4->CCR1 =0;}
        while (GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0))
        {counter=1;}

        while ((!GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0))&&(counter==1)) {
		TIM4->CCR1 = 2099; // 0.75V
              	}
        while (GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0))
        {counter=2;}
        
        while ((!GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0))&&(counter==2))
        {TIM4->CCR1 = 4199; // 1.5V
        } 

 	while (GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0)) {
	counter=3;	               
	}
        while ((!GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0))&&(counter==3))
        {TIM4->CCR1 = 6299; // 2.25V 
        }

	while (GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0)){
                counter=4;      
	}
        while ((!GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0))&&(counter==4))
        {TIM4->CCR1 = 8399; // 3V 
        }
        while (GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0)){
                counter=0;
                
        }
        
    }
}

