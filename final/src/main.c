#include <stdio.h>
#include "stm32f4xx.h"
#include "stm32f4xx_adc.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_rng.h"
#include "stm32f4xx_syscfg.h"

#define OPTIMAL 00
#define HEATED  01
#define OVERHEAT  10
#define TRIP  11

#define OPTIMAL_TEMP 34
#define HEATED_TEMP 36
#define OVERHEATED_TEMP 38

#define OPTIMAL_SPEED 5250
#define HEATED_SPEED 6720
#define OVERHEATED_SPEED 8400

const uint16_t LEDS = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
const uint16_t RELAY1 = GPIO_Pin_14;
const uint16_t RELAY2 = GPIO_Pin_15;

void init_leds();
void init_adc();
void init_timer_pins();
void init_timer();
void init_pwm();
void init_button();
void init_relay();
void read_adc();
static void InitSystick();
float Temperature_d;
float Temperature_v ;
float Temperature;

uint8_t state = OPTIMAL;

// SysTick Handler (ISR for the System Tick interrupt)
void SysTick_Handler(void)
{

  GPIO_ResetBits(GPIOD, GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);
  read_adc();
  switch(state)
  {

    case OPTIMAL:
        GPIO_SetBits(GPIOD, GPIO_Pin_12); //green LED
        GPIO_SetBits(GPIOC, RELAY2); //Reset Trip LED
	      GPIO_SetBits(GPIOC, RELAY1); //Reset Alarm LED
        if (Temperature > OPTIMAL_TEMP)
        {
	          TIM4->CCR1=HEATED_SPEED ;//speed-2 (75% of max speed)
            state = HEATED;
	      }
    break;

    case HEATED:
        GPIO_SetBits(GPIOD, GPIO_Pin_13);  //orange LED
        GPIO_SetBits(GPIOC, RELAY2); //Reset Trip LED
  	    GPIO_SetBits(GPIOC, RELAY1); //Reset Alarm LED
        if (Temperature > HEATED_TEMP )
        {
	         GPIO_SetBits(GPIOC, RELAY2); //Reset Trip
           GPIO_ResetBits(GPIOC, RELAY1); //Alarm Indication
	         state = OVERHEAT;
           TIM4->CCR1= OVERHEATED_SPEED;//speed-3 (100% of max speed)
        }
	      else if (Temperature < OPTIMAL_TEMP)
        {
	        state=OPTIMAL;
          TIM4->CCR1= OPTIMAL_SPEED; //speed-1 (50% of max speed)
          GPIO_SetBits(GPIOC, RELAY1);
        }
    break;

    case OVERHEAT:
       GPIO_SetBits(GPIOD, GPIO_Pin_14);  //RED led
       if (Temperature > OVERHEATED_TEMP)
       {
	        GPIO_ResetBits(GPIOC, RELAY1); //Alarm signal
	        GPIO_ResetBits(GPIOC, RELAY2); //Trip Signal
          state= TRIP;
	     }
       else if (Temperature < HEATED_TEMP )
       {
          GPIO_SetBits(GPIOC, RELAY2); //Reset Trip LED
	        state = HEATED;
          TIM4->CCR1= HEATED_SPEED; //Speed-2 (75% of max speed)
       }
    break;

   case TRIP:
       GPIO_SetBits(GPIOD, GPIO_Pin_15); //blue LED
       if (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0))
       {
          state=OPTIMAL;
          GPIO_SetBits(GPIOC, RELAY1);
	        GPIO_SetBits(GPIOC, RELAY2);
       }
   break;

   default:

        TIM4->CCR1=OPTIMAL_SPEED; //speed-1(50% of max speed)
        state = OPTIMAL;
	      break;
  }
}

// Initialize the system tick
void InitSystick(void)
{
	SystemCoreClockUpdate();
	if (SysTick_Config(SystemCoreClock / 100))
  {
		while (1);
	}
}


void init_leds()
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	GPIO_InitTypeDef gpio;
	GPIO_StructInit(&gpio);
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(GPIOD, &gpio);
}

void init_adc()
{
  ADC_InitTypeDef ADC_init_structure; //Structure for adc confguration
	GPIO_InitTypeDef GPIO_initStructre; //Structure for analog input pin
	//Clock configuration
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE); //The ADC1 is connected the APB2 peripheral bus thus we will use its clock source
	RCC_AHB1PeriphClockCmd(RCC_AHB1ENR_GPIOCEN, ENABLE); //Clock for the ADC port
	//Analog pin configuration
	GPIO_StructInit(&GPIO_initStructre);
	GPIO_initStructre.GPIO_Pin = GPIO_Pin_0; //The channel 10 is connected to PC0
	GPIO_initStructre.GPIO_Mode = GPIO_Mode_AN; //The PC0 pin is configured in analog mode
	GPIO_initStructre.GPIO_PuPd = GPIO_PuPd_NOPULL; //no pull up or pull down
	GPIO_Init(GPIOC, &GPIO_initStructre); //Affecting the port with the initialization structure configuration
	//ADC structure configuration
	ADC_DeInit();
	ADC_init_structure.ADC_DataAlign = ADC_DataAlign_Right; //data converted will be shifted to right
	ADC_init_structure.ADC_Resolution = ADC_Resolution_12b; //Input voltage is converted into a 12bit number giving a maximum value of 4096
	ADC_init_structure.ADC_ContinuousConvMode = ENABLE; //the conversion is continuous, the input data is converted more than once
	ADC_init_structure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1; // conversion is synchronous with TIM1 and CC1
	ADC_init_structure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None; //no trigger for conversion
	ADC_init_structure.ADC_NbrOfConversion = 1;
	ADC_init_structure.ADC_ScanConvMode = DISABLE; //The scan is configured in one channel
	ADC_Init(ADC1, &ADC_init_structure); //Initialize ADC with the previous configuration
	//Select the channel to be read from
	ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 1, ADC_SampleTime_144Cycles);
  //Enable ADC conversion
	ADC_Cmd(ADC1, ENABLE);

}

void init_relay()
{
	//RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	GPIO_InitTypeDef gpioc;
	GPIO_StructInit(&gpioc);
	gpioc.GPIO_OType = GPIO_OType_PP;
	gpioc.GPIO_Mode = GPIO_Mode_OUT;
	gpioc.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(GPIOC, &gpioc);
}

void read_adc()
{
	ADC_SoftwareStartConv(ADC1);//Start the conversion
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC));//Processing the conversion
	Temperature_d = ADC_GetConversionValue(ADC1); //Return the converted data
  Temperature_v = ((Temperature_d*3.3)/(float)4095);
  Temperature= (Temperature_v-0.4)/0.0195; //Sensitivity is 19.5 mV/degree celcius;400mV for 0 deg celcius
}

void init_timer_pins(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  /* Alternating functions for pins */
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_TIM4);
  /* Set pins */
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6;
  GPIO_InitStruct.GPIO_OType =GPIO_OType_PP; //64 for open drain
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOB, &GPIO_InitStruct);
}

void init_timer(void)
{
  TIM_TimeBaseInitTypeDef TIM_BaseStruct;
  /* Enable clock for TIM4 */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
  TIM_BaseStruct.TIM_Prescaler = 0;
  /* Count up */
  TIM_BaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_BaseStruct.TIM_Period = 8399; /* 10kHz PWM */
  TIM_BaseStruct.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_BaseStruct.TIM_RepetitionCounter = 0;
  /* Initialize TIM4 */
  TIM_TimeBaseInit(TIM4, &TIM_BaseStruct);
  /* Start count on TIM4 */
  TIM_Cmd(TIM4, ENABLE);
}

void init_pwm(void)
{
    TIM_OCInitTypeDef TIM_OCStruct;
    TIM_OCStruct.TIM_OCMode = TIM_OCMode_PWM2;
    TIM_OCStruct.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCStruct.TIM_OCPolarity = TIM_OCPolarity_Low;
    TIM_OCStruct.TIM_Pulse = 8399; /* PB6, 100% As defult */
    TIM_OC1Init(TIM4, &TIM_OCStruct);
    TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);
}

void init_button()
{
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  GPIO_InitTypeDef gpio;
	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = GPIO_Pin_0;
	gpio.GPIO_Mode = GPIO_Mode_IN;
	GPIO_Init(GPIOA, &gpio);
}


int main()
{
	InitSystick();
        init_leds();
	init_adc();
        init_timer_pins();
        init_timer();
        init_pwm();
        init_button();
        init_relay();
	GPIO_SetBits(GPIOC, RELAY1);
	GPIO_SetBits(GPIOC, RELAY2);
	while(1);
}
