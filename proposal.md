Project Proposal for EL6483
==========================

## Team

- Parth Ranjan Singh <mailto:prs331@nyu.edu>

- Rose Merlyn Prakash <mailto:rmp433@nyu.edu>

- Kedao Qi <mailto:kq286@nyu.edu>


## Idea

Our Project utilizes the PWM to control the speed of a cooling system (motor) w.r.t. temperature and alarm and trip control using relay protection in case of over-heating.
The motivation behind this Project is to control cooling of electronic boards on a rack when the system gets heated up during testing.
All of us are working in Power Lab and we constantly have the problem of system failure due to improper thermal management,especially during continuous testing.
So, our Project will be a miniature prototype for cooling system/proper thermal managment with optimized power usage.
This will also sense if temperature of any critical board/IC rises beyond a threshold and cuts off power.
This will ensure safety of Lab while we do over-night testings which is very common.

## Materials

The STM32F4 Discovery board is the primary micro-controller in our project.

External peripherals used are as follows:

- [24 V DC Fan](http://www.digikey.com/product-detail/en/ME80252V2-000U-A99/259-1507-ND/2021135)
- [110V AC - Variable DC Power Supply](http://www.amazon.com/Tekpower-Linear-TP1502D-Digital-Display/dp/B00MMOC9N8) (Power supply for the motor driver circuit and fan.)
- [External temparature sensors](http://www.digikey.com/product-detail/en/MCP9700A-E%2FTO/MCP9700A-E%2FTO-ND/1212508)
- [Motor shield]('http://arduino.cc/en/Main/ArduinoMotorShieldR3') (Direction control of the motor(fan) and also providing isolation to the microcontroller)
- [Relay]('http://www.digikey.com/product-detail/en/9007-05-00/306-1062-ND/301696') (Enable the Alarm and Trip Circuits as the supervisory control)

## Milestones

###Milestone 1 :


* Demonstrate STM32F4 board can control a DC motor for different PWM duty cycles

* Demonstrate STM32F4 board can sense Temparature from external sensors and print it

* Demonstrate STM32F4 board can generate IOs to relays in-order to give indication/alarm

###Milestone 2 :


* Demonstrate the complete working of Project - STM board should be able to sense temparature from external sensors
* Based on the temperature and required logic, STM board should control the speed of the cooling system and also have a supervisory control w.r.t the Alarm and Trip conditions


## Plan of work

###For First Milestone:

- #### Kedao

Responsible for implementing PWM control of DC motor with the STM board (Others will help)

```sh
     Week 1 Activities: Identify the PWM interfaces required on board and start coding/checking drivers for it
     Week 2 Activities: Implement the PWM control in software
     Week 3 activities: Interface the motors to GPIO ports of STM interface
     Week 4 activities: Set up/test/debug the working PWM control of motors with different value
```

- #### Rose

Responsible for implementing temperature sensing using external sensors and STM board (Others will help)
```sh
     Week 1 Activities: Identify the board interfaces for external temperature sensors and start coding/checking drivers for it
     Week 2 Activities: Implement the temparature sense software in STM board using ADC
     Week 3 Activities: Setup Interfacing of the external sensors to STM board
     Week 4 Activities: Setup/test/debug the working temperature sensing
```

- #### Parth

Responsible for implementing shield based motor control and relay Protection (Others will help)
```sh
      Week 1 Activities: Identify the board interfaces for relay IOs and and start coding/checking drivers for it
      Week 2 Activities: Implement the relay protection software in STM board
      Week 3 Activities: Setup Interfacing of the shield to STM board
      Week 4 Activities: Setup/test/debug the working relay protection for a specified temperature value and LED indications for alarm and trip conditions
```

###For second milestone:

#####Combined Team Effort

```sh
 Week 5&6: Three of us will be integrating/testing our tasks together to deliver the Project

```
